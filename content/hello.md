---
title: Home
date: 2021-10-11T13:58:28.190Z
description: Welcome
---
## Have you ever noticed this thing with personal websites?

Every time you update them, you think _wow, how much time has passed?_

Today is August 7, 2020\. Most countries are still in lock down. I sit in a coworking space, just finished all tasks for today. Time to look into all the leftover tasks from the last months.

This pops up: **“Simplify my personal website.”**

I never saw the importance of personal websites. I don’t even know how much traffic comes on this one. Probably less than what would justify putting so much effort into it.

## This website is like a time machine

I update it every couple of years, and every time I can look back:

*   It shows me where I’ve come from.
*   What person I was 3 years ago.
*   And: how foolish I thought.

## You might think: great, but why should _I_ care?

Well, this website is not here persuade you of anything.

It’s not here to sell you anything.

–> It is here to make a new friend. Or to leave it.

## Yes, this _IS_ for you

May I assume your time is precious?

May I assume you get more facebook friend requests than you accept?

Then this website helps you to decide if we have a value match.

If there’s a point in reaching out to Till.

So: go for it: read my [values](/connect) and see if there’s a match!
