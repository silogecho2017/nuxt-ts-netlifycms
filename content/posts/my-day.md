---
title: My day
description: Why today was a perfect day, Todays blog
image: https://images.unsplash.com/photo-1593642533144-3d62aa4783ec?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2069&q=80
---

Today was a perfect day in all aspects. Let's discuss why today was perfect:

- I had fun
- I was not bored

It's hard to disagree, you have to agree with me on this.

If you check the markdown files of these blogs, you'll notice that there's no date of any kind. That's because Nuxt content automatically tracks when the file was created and updated.
"Time for a shower." I'm always tired in the mornings and find getting up for a shower really horrible, though it does a good job of waking me up and after it I'm ready for another day at school. Well that is what I think but I usually end up in bed asleep right up until 7.45 am. Luckily I have usually packed my school stuff the night before.

My house is quite big and there is lots of room to get away from the rest of my family. I have a computer in my room and I spend quite a lot of time on…show more content…
Another couple of friends arrive at this house too and we all get together and walk to school. This walking to school together gives me a good opportunity to make after school arrangements. Once at school (the walk takes about 15 minutes) I go to my tutor base and catch up with my friends who don't walk to school with me. After a short rest I make my way to my first lesson on a Monday usually English.
