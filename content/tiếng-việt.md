---
title: Tiếng Việt
date: 2021-10-11T14:03:56.527Z
description: Tiếng Việt
---
![](/images/tillcarlos/tiengviet/en.png) For English speakers: this page is a summary of all other pages, translated into Vietnamese. As I stay in Vietnam for several months every year, I get enquiries from Vietnamese people. This is my attempt to answer questions only once.

## ![](/images/tillcarlos/tiengviet/vn.png) Nội dung của trang này

Đây là trang chủ cá nhân của tôi. Tôi sử dụng trang này để cung cấp cho bạn một cái nhìn khái quát về những gì tôi đang làm và cách để kết nối với tôi hiệu quả nhất.

Tóm tắt nhanh: Tôi tên là Till Carlos, là một doanh nhân, YouTuber và một nhà tiếp thị số. Tôi điều hành công việc của mình từ xa và đi du lịch đến nhiều nơi trên thế giới. Tôi nói được bốn thứ tiếng bởi vì tôi tò mò về cách kết nối với những người mà tôi có thể học hỏi được từ họ.

## Chuyên môn của bạn là gì?

Tôi làm việc trong lĩnh vực Tiếp thị số-Digital Marketing (vâng, bạn có thể thuê tôi). Tôi điều hành việc kinh doanh riêng trong lĩnh vực phần mềm (SasS = Software-as-a-Service).

Trước đó tôi đã từng quản lý nhiều dự án IT và điều hành sự nghiệp riêng của mình một cách song song. Nếu bạn cần hỗ trợ trong bất kỳ lĩnh vực nào kể trên, bạn hãy vui lòng [liên lạc với tôi](/connect) nhé!

![](/images/tillcarlos/tiengviet/sledge.jpg)

## Giới thiệu về Till

Tên tôi là Till Carlos, sinh năm 1984 tại Kiel, nước Đức. Tôi đã học về khoa học máy tính và bắt đầu dự án đầu tiên vào năm 2007.

Sau khi tốt nghiệp đại học năm 2012, tôi bắt đầu đi du lịch và không ở cố định bất kì nơi nào. Tôi giữ cho đồ dùng cá nhân của mình thật tối giản và học cách sống với rất nhiều sim điện thoại, những địa chỉ thay đổi liên tục và hạn chế những sự xáo trộn trong cuộc sống của mình.

### Bạn đang làm gì ở Việt Nam?

Tôi đến Việt Nam lần đầu tiên vào năm 2013, vì phải “chạy visa” từ Thái Lan. Và rồi tôi bắt đầu yêu đất nước này và quyết định quay lại nhiều nhất có thể.

Khi viết những dòng này vào mùa hè năm 2017, tôi đã đến thăm Việt Nam khoảng 10 lần và dành khoảng tất cả là 20 tháng ở đấy. Tôi đi du lịch nhiều nơi, nhưng dành phần lớn thời gian Thành phố Hồ Chí Minh.

![](/images/tillcarlos/tiengviet/scooter.jpg)

### Bạn đến từ đâu?

Kiel, nước Đức. Nó gần với biên giới Đan Mạch. Đúng thế, nơi đó từng thuộc về Tây Đức.

### Bạn đã học Tiếng Việt như thế nào?

Tôi chưa từng đến trường học một cách bài bản nhưng tôi có những người bạn rất tuyệt đã dạy cho tôi. Hầu như tôi chỉ học bằng cách ngồi cạnh những người Việt Nam không nói tốt hoặc không thể nói Tiếng Anh.

Trong những tháng đầu tiên, tôi chỉ có thể nghe thôi.

Tôi mất một năm để nghe và phát âm các dấu.

Và đến hiện tại tôi vẫn cần phải cải thiện rất nhiều. Có lẽ quá trình đó sẽ không bao giờ dừng lại.

### Tôi có thể hẹn hò với bạn không?

Không.

### Tiếng Việt của bạn vẫn chưa tốt. Tôi có thể dạy cho bạn không?

Vâng. Hãy gửi cho tôi một email và cho tôi biết phương pháp của bạn. Vui lòng viết bằng Tiếng Việt và bắt đầu chầm chậm.

### Tôi có thể làm việc cho bạn không?

Có thể. Việc đó phụ thuộc vào những vị trí còn trống của chúng tôi.

Hãy gửi cho tôi một email và nói về lĩnh vực mà bạn thông thạo. Vui lòng viết bằng Tiếng Anh. Dù tôi có thể nói và viết Tiếng Việt nhưng những tài liệu làm việc của chúng tôi đều được viết bằng Tiếng Anh. Đó là lí do tại sao viết Tiếng Anh là một yêu cầu cần thiết.