---
title: User Manual
date: 2021-10-11T14:04:25.990Z
description: User Manual
---

> “I learned so much about you after reading this, everyone should write one of these for themselves” _~Anonymous reader of this text_

## What’s a _user manual_ for a person?

In my last years as a software developer, entrepreneur, and friend I have driven quite a few people crazy.

At the same time I have helped team members achieve more than they thought would be possible.

I started my first company in 2009, which is 11 years ago, as of this writing.

Time to reflect is never wrong, especially after one made leaps in their career.

Do I know myself better now?

A good friend sent me [this course](https://productize.co/build-your-product-program/), which led me to this blog post about [writing your own user manual](https://feld.com/archives/2016/04/user-manual-working.html).

## Part 1: Know more about me

If you work with me, you should read this.

Of course, it’s even better if you plan to work with me.

Now, let me go through the questions from the blog post above.

### What are some honest, unfiltered things about you?

I swing back and forth between extreme extroversion (even showing off) and then rather introverted building things. I was like that as a kid playing Lego, and I’m still the same now playing [Ruby on Rails](https://rubyonrails.org/).

If I’m not being challenged or in awe my attention span is super short. That was the case even before smartphones collectively reduced our attention span. Being with someone boring lasts for exactly 22.3 minutes, then I’m out. I got some shit for [this video](https://www.youtube.com/watch?v=Rn4Daxfr4cU) from 2017 where I said I only date girls speaking Vietnamese with me. It’s not meant to be mean, it’s just my way to stay focused in a conversation.

Laziness. People say I “work like a dog”, but the ultimate goal is automation and leveraging my own time. There are enough things I don’t like to do, including cooking, writing invoices, cleaning, and project management. If you think I like project management and I will take care of it, you will see the reality soon.

More specifically: I can be systematic, especially in the beginning of a project. My energy level drops fast after the initial phase. If you could only know one thing about me, **this would be it!**

### What drives you nuts?

In 2017 I stopped drinking alcohol altogether. It’s just not good for my body, and honestly: I never liked it. Discussions about that which are in any way negative or “come on, have _one_”, annoy the heck out of me. I have no issue with you drinking, but if you try to make me drink you’ll lose a friend.

Another thing is laziness. I cannot comprehend how people squander their talents by doing things that don’t contribute to a better life of their own or others. I see this a lot in the ongoing inclusivity debates. If you know me just a bit you will see that I love all people. I wish for a world where the color of our skin or ethnicity is like hair color: totally meaningless.

How does this connect to laziness? People who want to police speech are in my eyes just selfish bureaucrats. People are scared nowadays by a wild mob on social media that does [not even read the sources](https://buttondown.email/hillelwayne/archive/please-read-the-paper-before-you-comment/)! Do you want to help [insert minority], then go make a friend of that minority, and help them lead a better life. Nobody is helped by rewriting software documentation etc. Go use that time, make money, and then donate that money.

Lacking futurism: Like laziness I hate it when there is shortsightedness. Every year when I visit Germany I am amazed how car brands manage to make facelifts, but nothing else. Then _dieselgate_ happened. And now Tesla is about to eat their lunch. No wonder! Then there is bureaucracy. I can work with it, and it’s a part of life. But I hate unnecessary complexity. That’s likely to be the reason why I left Germany.

### What are your quirks?

I often browse [9gag](https://9gag.com/) and laugh about stuff that’s way to dark to be published.

After every three months of staying at one place, I need to get out and be with myself for a week or so. I love sitting in a cafe in a city I haven’t seen yet, and just work on my stuff - undistracted.

You will almost always see me walking around with a [Minaal v2 backpack](https://www.minaal.com/products/minaal-carry-on-bag), because I rather stay all day outside then to drop my bag at home. Part of my backpack is a small nail clipper I bought 10 years ago, which I use often. I hate typing with long fingernails.

Fashion is not my strong suit. I wear what is practical. If there’s an item I like I try to keep it for a long time.

### How can people earn an extra gold star with you? ⭐️

If you take care of things. Doing things on time. And if you cannot do them, let me know that there’s a delay and do it later. I don’t care if things are late, as long as I don’t have to remind you. Give me status updates! **If I need to remind you = problems ahead.**

### What qualities do you particularly value in people who work with you?

I like people with a certain attitude towards life and work. I care more about input than output. If you are willing to do the work, you will have all my support.

### What are some things that people might misunderstand about you that you should clarify?

Yes, I am German. No, I don’t like beer. No, I also don’t like football.

## Part 2: How to work with me

### How do you coach people to do their best work and develop their talents?

Understand this first: I am very good at learning fast. And it’s hard for me to achieve deep understanding. I have seen that at various sports I tried, it’s a repeating pattern. I often don’t know how I get to a solution, I have an intuitive feeling of it very fast.

This makes me a poor teacher. I tried teaching English for one month, and it did not go very well (at least in my eyes).

How I coach people roughly goes like this:

1.  I set a goal which I know I could achieve in a certain time
2.  I give you time
3.  I give you resources

The core is this: My strength is to **show you that it’s possible**, not to tell you how to get there. If you have doubts about yourself, come back and I help you solve the issue. I won’t check on your progress. If you do that yourself: ⭐️ (see above)

### What’s the best way to communicate with you?

Never say “Hello, how are you?” and then wait. I will not respond.

If you want to jump on a zoom call: **Chat with me first!!** Never just call me out of the blue. It’s impolite, and zoom will pop up while I type a sentence, and then the call starts already without me being able to stop it.

### What’s the best way to convince you to do something?

Show me the data. Show me how this helps me achieve my goals / the company’s goals.

### How do you like to give feedback?

I am straightforward and ruthless. I can tell you that “your website is shit” and I mean it 0% personal.

One policy of mine is this: I don’t discuss something about someone unless I have already told them that before myself.

### How do you like to get feedback?

Same as how I give feedback: ruthless and honest. My guess is that there is 0 way to really upset me or hurt my feelings. If you find one, I’ll give you an award and will amend this blog post.

## After writing this

Wow, this was fun. I really don’t know the effects of this, but I’m gonna share it with some people.

How did you like it? Let me know: [till@tillcarlos.com](mailto:till@tillcarlos.com)
